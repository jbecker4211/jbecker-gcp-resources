provider "google" {
credentials = "${file("/Users/jamesbecker/crlab.json")}"
project = "${var.cloudreach_project}"
region = "${var.cloudreach_region}"
}

data "google_compute_network" "deployment-network"{
    name = "prodvpc"
}


resource "google_compute_subnetwork" "clustersubnetwork" {

    name = "${var.cloudreach_subnet_name}"
    ip_cidr_range = "${var.cloudreach_subnet_ip_cidr_range}"
    network = "${data.google_compute_network.deployment-network.self_link}"
    region = "${var.cloudreach_region}"
    description = "subnet to deploy kubernetes cluster to"
    project = "${var.cloudreach_project}"
    private_ip_google_access = "true"
    enable_flow_logs = "false"
    secondary_ip_range = "${var.cloudreach_subnet_secondary_list}"
}



resource "google_container_cluster" "default" {
    name = "${var.cloudreach_k8_name}"
    zone = "${var.cloudreach_k8_zone}"
    additional_zones = ["${var.cloudreach_k8_additional_zones}"]
    addons_config {
        horizontal_pod_autoscaling{
            disabled =  "false"
        }
        http_load_balancing{ 
            disabled = "false"
         }
        kubernetes_dashboard{
            disabled = "false"
         }
        network_policy_config{
            disabled = "false"
        }
    }
    master_auth  {
        username = "${var.cloudreach_k8_username}"
        password = "${var.cloudreach_k8_password}"
    }
    initial_node_count = "${var.cloudreach_k8_initial_node_count}"
    cluster_ipv4_cidr = "${var.cloudreach_k8_cluster_ipv4_cidr}"
    description = "${var.cloudreach_k8_description}"
    enable_kubernetes_alpha = "false"
    enable_legacy_abac = "false"
    ip_allocation_policy{
        cluster_secondary_range_name = "${lookup(google_compute_subnetwork.clustersubnetwork.secondary_ip_range[0], "range_name")}"
        services_secondary_range_name = "${lookup(google_compute_subnetwork.clustersubnetwork.secondary_ip_range[1],"range_name")}"
    }
    maintenance_policy{ 
        daily_maintenance_window { 
            start_time = "${var.cloudreach_k8_maintenance_window}"
        }
    }
    master_authorized_networks_config{
        cidr_blocks {
           cidr_block = "${var.cloudreach_k8_master_authorized_blocks}"
        }
    }
    master_ipv4_cidr_block = "${var.cloudreach_master_cidr_block}"
    network = "${data.google_compute_network.deployment-network.self_link}"
    network_policy {
        provider = "PROVIDER_UNSPECIFIED"
        enabled = "false"
    }
    pod_security_policy_config {
        enabled = "false"
    }
    node_config {
        tags = ["bastionmanaged","nat"]
    }
    private_cluster = "${var.cloudreach_k8_private_cluster}"
    project = "${var.cloudreach_project}"
    remove_default_node_pool = "false"
    subnetwork = "${google_compute_subnetwork.clustersubnetwork.self_link}"
}
