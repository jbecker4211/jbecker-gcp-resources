
## Module variables
cloudreach_project = "cr-celab"
cloudreach_region = "us-central1"

## Subnetwork 1
cloudreach_subnet_name = "kubernetessubnet"
cloudreach_subnet_ip_cidr_range  = "10.2.0.0/16"
cloudreach_subnet_secondary_list = [{"ip_cidr_range"="10.4.0.0/16","range_name"="secondary1"},{"ip_cidr_range"="10.3.0.0/16","range_name"="secondary2"}]

## GKE variables 

cloudreach_k8_name = "jbeckerk8cluster"
cloudreach_k8_zone = "us-central1-a"
cloudreach_k8_initial_node_count = "2"
cloudreach_k8_additional_zones = ["us-central1-b","us-central1-c"]
cloudreach_k8_username = "jbecker"
cloudreach_k8_password = "P@ssw0rd1234567890"
cloudreach_k8_cluster_ipv4_cidr = "10.1.1.0/24"
cloudreach_k8_description = "jbeckerk8cluster"
cloudreach_k8_maintenance_window = "03:00"

#### THIS NEEDS TO BE THE PUBLIC IP OF WHATEVER IP YOU ARE ACCESSING THE CLUSTER FROM
cloudreach_k8_master_authorized_blocks = "10.250.0.0/16"


cloudreach_master_cidr_block = "10.5.0.0/28"

#### This makes the cluster private and does not give public IPs to the nodes in the cluster
#### If true, a NAT will need to be deployed to give access to the internet for the nodes
cloudreach_k8_private_cluster = "true"

