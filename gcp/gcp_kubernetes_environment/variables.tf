variable "cloudreach_project" {

    type = "string"
    description = "project to deploy to"
}


###################################################
######### variables for google_compute_cloud_subnet
###################################################
#### REQUIRED
variable "cloudreach_subnet_name" {
    description = "name of compute network"
    type = "string"
}

variable "cloudreach_subnet_ip_cidr_range" {
    description = "cidr range for subnet"
    type = "string"
}



variable "cloudreach_region" {
    description = "region to deploy the subnet"
    type = "string"
}

#### OPTIONAL


variable "cloudreach_subnet_secondary_list" {
    description = "an array of configurations for secondary IP ranges for VM instances contained in this subnetwork."
    type = "list"
    default = []
    
        ## Secondary IP range block supports the following:
        ### range_name -- the name associated with the subnetwork
        ### ip_cidr_range -- the range of the ip addresses belonging to this subnetwork
}

############################################
####### VARIABLES FOR GKE
############################################

#### REQUIRED
variable "cloudreach_k8_name"{
type = "string"
description = "name of the cluster"
}



#### OPTIONAL NO DEFAULT
variable "cloudreach_k8_zone"{
type = "string"
description = "zone for deployment"
}


variable "cloudreach_k8_additional_zones"{
type = "list"
description = "other zones to deploy to"
}

variable "cloudreach_k8_username"{
type = "string"
description = "master username"
}

variable "cloudreach_k8_password" {
type = "string"
description = "password for master"
}

variable "cloudreach_k8_master_authorized_blocks"{
type = "string"
description = "list of cidr block to allow traffic from.  to make cluster private omit line"
}

variable "cloudreach_master_cidr_block"{
type = "string"
description = "cidr block to use for master subnet range.  must be a /28 network"
default = "10.0.0.0/28"
}


variable "cloudreach_k8_cluster_ipv4_cidr"{
type = "string"
description = "ipv4 cidr for the pod network"
}

variable "cloudreach_k8_description" {
type = "string"
description = "cluster description"
}

variable "cloudreach_k8_initial_node_count"{

    type = "string"
    description = "number of nodes to deploy"
}

#### OPTIONAL DEFAULT

variable "cloudreach_k8_maintenance_window"{
type = "string"
description = "time to start maintenance"
default = "03:00"
}


variable "cloudreach_k8_private_cluster"{
type = "string"
description = "is the cluster private or not"
}
