provider "google" {
credentials = "${file("/Users/jamesbecker/crlab.json")}"
project = "${var.cloudreach_project}"
region = "${var.cloudreach_region}"
}

#### SUBNETWORK IDENTENTITY 
data "google_compute_subnetwork" "bastion-subnet"{

    name = "bastion-1"
    region = "us-central1"

}

data "google_compute_network" "bastion-network"{

    name = "bastionvpc"
    project = "cr-celab"

}


#### BASTION INSTANCE
resource "google_compute_instance" "bastion" {
    name = "${var.cloudreach_instance_name}"
    machine_type = "n1-standard-1"
    zone = "${var.cloudreach_zone}"

    network_interface {
        subnetwork = "${data.google_compute_subnetwork.bastion-subnet.self_link}"
        subnetwork_project = "${var.cloudreach_project}"
        access_config {
            network_tier = "PREMIUM"
        }
    }
    boot_disk{
        auto_delete ="true"
        device_name = "bastion_boot_disk"
        initialize_params {
            type = "pd-standard"
            image = "jbecker-bastion-dev"
        }
    }

    allow_stopping_for_update = "true"
    can_ip_forward = "false"
    description = "bastion instance"
    deletion_protection = "false"
    project = "${var.cloudreach_project}"
    scheduling {
        preemptible = "false"
        on_host_maintenance = "MIGRATE"
        automatic_restart = "true"
    }
    tags = ["bastion"]

}
#### FIREWALL RULE FOR SSH ACCESS
resource "google_compute_firewall" "bastion22" {

    name = "allow-ssh-bastion"
    network = "${data.google_compute_network.bastion-network.self_link}"
    source_ranges = "${var.cloudreach_firewall_source_ranges}"
    allow {
        protocol = "TCP"
        ports = ["22"]
    }

    description = "Bastion Instance"
    disabled = "false"
    project = "${var.cloudreach_project}"
    priority = "1000"
    target_tags = ["bastion"]
    direction = "INGRESS"


}
