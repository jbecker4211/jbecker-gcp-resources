##########################################
########### VARIABLES FOR GKE and BASTION
##########################################


variable "cloudreach_instance_name"{
description = "name of instance"
type = "string"
}

variable "cloudreach_zone"{
description = "zone to deploy instance to"
type = "string"
}

variable "cloudreach_project" {

    description = "project to associate with"
    type = "string"

}

variable "cloudreach_region" {

    description = "region to deploy to"
    type = "string"

}

variable "cloudreach_firewall_source_ranges" {

    description = "list of ip ranges to allow traffic from"
    type = "list"

}
