## Variables
cloudreach_project = "cr-celab"
cloudreach_region = "us-central1"
cloudreach_zone = "us-central1-a"

## Bastion Host Instance
cloudreach_instance_name = "jbbastion"


## list of ranges to allow traffic from
cloudreach_firewall_source_ranges = ["0.0.0.0/0"]
