provider "google" {
credentials = "${file("/Users/jamesbecker/crlab.json")}"
project = "${var.cloudreach_project}"
region = "${var.cloudreach_region}"
}

#### SUBNETWORK IDENTENTITY 
data "google_compute_subnetwork" "docker-subnet"{

    name = "production-us-west1-9"
    region = "us-west1"

}

data "google_compute_network" "docker-network"{

    name = "prodvpc"
    project = "cr-celab"

}

#### DOCKER INSTANCE INSTANCE
resource "google_compute_instance" "docker" {
   count = 3
    name = "${var.cloudreach_instance_name}-${count.index}"
    machine_type = "n1-standard-1"
    zone = "${var.cloudreach_zone}"

    network_interface {
        subnetwork = "${data.google_compute_subnetwork.docker-subnet.self_link}"
        subnetwork_project = "${var.cloudreach_project}"
#        access_config {
#            network_tier = "PREMIUM"
#        }
    }
    boot_disk{
        auto_delete ="true"
        device_name = "docker_boot_disk"
        initialize_params {
            type = "pd-standard"
            image = "centos-cloud/centos-7"
        }
    }

    allow_stopping_for_update = "true"
    can_ip_forward = "false"
    description = "docker instance"
    deletion_protection = "false"
    project = "${var.cloudreach_project}"
    scheduling {
        preemptible = "false"
        on_host_maintenance = "MIGRATE"
        automatic_restart = "true"
    }
    metadata {
        ssh-keys = "${var.cloudreach_login_name}:${var.cloudreach_ssh_key}"
    }
    tags = ["bastionmanaged","nat"]

}
