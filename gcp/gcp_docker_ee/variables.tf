##########################################
########### VARIABLES FOR GKE and BASTION
##########################################


variable "cloudreach_instance_name"{
description = "name of instance"
type = "string"
}

variable "cloudreach_zone"{
description = "zone to deploy instance to"
type = "string"
}

variable "cloudreach_project" {

    description = "project to associate with"
    type = "string"

}

variable "cloudreach_region" {

    description = "region to deploy to"
    type = "string"

}

variable "cloudreach_ssh_key"{
    
    description = "ssh key access key for metadata"
    type = "string"

}
