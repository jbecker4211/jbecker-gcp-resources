provider "google" {
credentials = "${file("/Users/jamesbecker/crlab.json")}"
region = "us-central-1"
}

#### FIREWALL RULE FOR SSH ACCESS
resource "google_compute_firewall" "bastion-access" {

    name = "bastion-ssh-access"
    network = "${google_compute_network.prodnetwork.self_link}"
    source_ranges = ["${var.cloudreach_subnet_ip_cidr_range_bastion}"]
    allow {
        protocol = "TCP"
        ports = ["22"]
    }

    allow {
        protocol = "icmp"
    }

    description = "Bastion managed instance"
    disabled = "false"
    project = "${var.cloudreach_project}"
    priority = "1000"
    target_tags = ["bastionmanaged"]
    direction = "INGRESS"


}

resource "google_compute_network" "prodnetwork" {

    name = "${var.cloudreach_prod_vpc_name}"
    auto_create_subnetworks = "false"
    routing_mode = "GLOBAL"
    description = "Production Network"
    project = "${var.cloudreach_project}"
}

resource "google_compute_network" "bastionnetwork" {

    name = "${var.cloudreach_bastion_vpc_name}"
    auto_create_subnetworks = "false"
    routing_mode = "GLOBAL"
    description = "Bastion Network"
    project = "${var.cloudreach_project}"

}
resource "google_compute_network_peering" "peera"{

    name = "${google_compute_network.prodnetwork.name}-peering"
    network = "${google_compute_network.prodnetwork.self_link}"
    peer_network = "${google_compute_network.bastionnetwork.self_link}"

    auto_create_routes = "true"

}

resource "google_compute_network_peering" "peerb" {

    name = "${google_compute_network.bastionnetwork.name}-peering"
    network = "${google_compute_network.bastionnetwork.self_link}"
    peer_network = "${google_compute_network.prodnetwork.self_link}"

    auto_create_routes = "true"

}

resource "google_compute_subnetwork" "prod_subnetworks" {

    count = 9
    name = "production-${element(var.cloudreach_subnet_region_production,count.index%3)}-${count.index+1}"
    ip_cidr_range = "${element(split(".",var.cloudreach_subnet_ip_cidr_range_production),0)}.${element(split(".",var.cloudreach_subnet_ip_cidr_range_production),1)}.${count.index}.0/24"
    network = "${google_compute_network.prodnetwork.self_link}"
    region = "${element(var.cloudreach_subnet_region_production,count.index%3)}"
    description = "production subnet ${count.index}"
    project = "${var.cloudreach_project}"
    private_ip_google_access = "true"
    enable_flow_logs = "false"
}

resource "google_compute_subnetwork" "bastion_subnetworks" {

    count = 3
    name = "bastion-${count.index+1}"
    ip_cidr_range = "${element(split(".",var.cloudreach_subnet_ip_cidr_range_bastion),0)}.${element(split(".",var.cloudreach_subnet_ip_cidr_range_bastion),1)}.1.${count.index*16}/28"
    network = "${google_compute_network.bastionnetwork.self_link}"
    region = "${element(var.cloudreach_subnet_region_bastion,count.index)}"
    description = "bastion subnet ${count.index}"
    project = "${var.cloudreach_project}"
    private_ip_google_access = "true"
    enable_flow_logs = "false"
}
