#### PLACE HOLDERS
variable "cloudreach_project" {
    description = "project name"
    type = "string"
    default = "cr-celab"
}

###########################################
######### variables for PROD VPC
############################################
#### REQUIRED
variable "cloudreach_prod_vpc_name" {

    description = "name of compute network"
    type = "string"

}

###########################################
######### variables for PROD VPC
############################################
#### REQUIRED
variable "cloudreach_bastion_vpc_name" {

    description = "name of compute network"
    type = "string"

}


######################################################
############ Variables for bastion network
######################################################

variable "cloudreach_subnet_region_bastion"{

    description = "regions to deploy bastion networks too"
    type = "list"

}

variable "cloudreach_subnet_ip_cidr_range_bastion" {

    description = "cidr range for subnet"
    type = "string"

}



#####################################################
################ Variables for prod subnet
#####################################################

variable "cloudreach_subnet_region_production"{
    
    description = "production regions"
    type = "list"

}

variable "cloudreach_subnet_ip_cidr_range_production"{

    description = "/16 for production traffic"
    type = "string"
}
