#output "project_id" {

#    value = "${google_project.networkproject.project_id}"

#}

output "prod_network_self_link" {

    value = "${google_compute_network.prodnetwork.self_link}"

}

output "bastion_network_self_link" {

    value = "${google_compute_network.bastionnetwork.self_link}"

}

output "prod_network_list" {

    value = ["${google_compute_subnetwork.prod_subnetworks.*.self_link}"]

}

output "bastion_network_list" {

    value = ["${google_compute_subnetwork.bastion_subnetworks.*.self_link}"]
}
