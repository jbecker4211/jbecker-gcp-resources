################# prod vpc ####################################

cloudreach_prod_vpc_name = "prodvpc"


################# bastion vpc ##################################

cloudreach_bastion_vpc_name = "bastionvpc"

################# Bastion subnetwork configurations #############

cloudreach_subnet_region_bastion = ["us-central1","us-west1","us-east1"]
cloudreach_subnet_ip_cidr_range_bastion = "10.250.0.0/16"

################## Production subnet configurations ##############

cloudreach_subnet_region_production = ["us-central1","us-east1","us-west1"]
cloudreach_subnet_ip_cidr_range_production = "10.1.0.0/16"


