provider "google" {
credentials = "${file("/Users/jamesbecker/crlab.json")}"
project = "${var.cloudreach_project}"
region = "${var.cloudreach_region}"
}

#### SUBNETWORK IDENTENTITY 
data "google_compute_subnetwork" "nat-subnet"{

    name = "production-us-east1-5"
    region = "us-east1"

}

data "google_compute_network" "nat-network"{

    name = "prodvpc"
    project = "cr-celab"

}

#### NAT INSTANCE INSTANCE
resource "google_compute_instance" "nat-gateway" {
    name = "${var.cloudreach_instance_name}"
    machine_type = "n1-standard-1"
    zone = "${var.cloudreach_zone}"

    network_interface {
        subnetwork = "${data.google_compute_subnetwork.nat-subnet.self_link}"
        subnetwork_project = "${var.cloudreach_project}"
        access_config {
            network_tier = "PREMIUM"
        }
    }
    boot_disk{
        auto_delete ="true"
        device_name = "nat_boot_disk"
        initialize_params {
            type = "pd-standard"
            image = "jb-nat-gateway"
        }
    }

    allow_stopping_for_update = "true"
    can_ip_forward = "true"
    description = "nat instance"
    deletion_protection = "false"
    project = "${var.cloudreach_project}"
    scheduling {
        preemptible = "false"
        on_host_maintenance = "MIGRATE"
        automatic_restart = "true"
    }
    metadata {
        ssh-keys = "${var.cloudreach_login_name}:${var.cloudreach_ssh_key}"
    }
    tags = ["bastionmanaged", "external"]

}


#### ROUTE FOR NAT
resource "google_compute_route" "nat-route"{
    name = "nat-route"
    dest_range = "0.0.0.0/0"
    network = "${data.google_compute_network.nat-network.name}"
    tags = ["nat"]
    priority = "900"
    next_hop_ip = "${google_compute_instance.nat-gateway.network_interface.0.address}"
    

}
