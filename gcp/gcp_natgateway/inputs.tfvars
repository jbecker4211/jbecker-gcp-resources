## Variables
cloudreach_project = "cr-celab"
cloudreach_region = "us-central1"
cloudreach_zone = "us-east1-b"

## Bastion Host Instance
cloudreach_instance_name = "jb-nat"



##### METADATA CONFIGURATION FOR SSH ACCESS
cloudreach_login_name = "jamesbecker"
cloudreach_ssh_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC5HgjtgMaUKrnJscbuUsexDfc3o0hhBH7je1PWY4tekJPom05+xB1CbO8d9Vtd+LLfEQptNNSQdQW51m1PpklotweoiTo+5KDqzlAGn7HBEMw8f6bAaZsxr3iE2JBLLC3BWZf6VzbkC5HWKIPdaI2f2Blnbl/y5/80ih8zkYwXRc4VgOSjSJA2rB53agH/bOTMJXSA2PWMxgkkOYU/kiKVo5Up+86V/WAnuOCD0DrBMr6fWK0qApnkeP/K+xQc+IE2AQCX88krcPh+WCo6k8MFILzAw/g51ez+HoSb9DeylCXQcMjuLf5NwDAGPWvqUrt0eNyd/Is3YL35eSlW0wK7 jamesbecker@jbbastion"
